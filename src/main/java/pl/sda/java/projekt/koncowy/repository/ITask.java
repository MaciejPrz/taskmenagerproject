package pl.sda.java.projekt.koncowy.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.java.projekt.koncowy.entity.Task;
import org.springframework.data.domain.Pageable;

@Repository
public interface ITask extends CrudRepository<Task,Long> {
    Page<Task> findAllByIsComplectedFalse(Pageable pageable);
    Page<Task> findAllByIsComplectedFalseOrderByDataAsc(Pageable pageable);

    //jeżeli zapytanie piszemy ręcznie to @Querry("SELECT * FROM ** WHERE **")
}
//Pageable dodanie stronicowania