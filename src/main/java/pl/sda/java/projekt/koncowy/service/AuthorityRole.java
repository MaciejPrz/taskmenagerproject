package pl.sda.java.projekt.koncowy.service;

public enum AuthorityRole {
    ADMINISTRATOR("Administrator"), MANAGER("Manager"), USER("User"), BLOCKER("Blocked");

    private String name;

    AuthorityRole(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
