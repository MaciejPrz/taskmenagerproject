package pl.sda.java.projekt.koncowy.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.sda.java.projekt.koncowy.entity.User;
import pl.sda.java.projekt.koncowy.model.UserFormModel;
import pl.sda.java.projekt.koncowy.repository.IUser;
import pl.sda.java.projekt.koncowy.service.AuthorityRole;


@Controller
public class UserControler {

    @Autowired
    IUser iUser;


    PasswordEncoder passwordEncoder= new BCryptPasswordEncoder();

    @RequestMapping(value = "/userList")
    public String view(Model model) {
        model.addAttribute("lista", iUser.findAllByOrderById());
        return "userList";
    }

    @GetMapping(path = "/addUser")
    public String addUser(Model model) {
        User newUser = new User();
        model.addAttribute(passwordEncoder);
        model.addAttribute("newUser",newUser);
        return"addUser";
        }


    @RequestMapping(value = "/saveNewUser", method = RequestMethod.POST)
    public String saveNewUser(@Validated User newUser, BindingResult errors) {
        if (errors.hasErrors()) {
            return "addUser";
        }
        newUser.setLogin(newUser.getLogin());
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        // entity.setPassword(userFormModel.getPassword());
        newUser.setAuthorityRole(newUser.getAuthorityRole());

        iUser.save(newUser);
       /* iUser.save(newUser);*/
        return "redirect:/userList";
    }

    @ModelAttribute("authorityRoleList")
    public AuthorityRole[] authorityRoleList() {
        return AuthorityRole.values();
    }

    @GetMapping(path = "/editUser/{UserId}")
    public String editUser(@PathVariable("UserId") Long UserId, Model model) {
        User user = iUser.findOne(UserId);
        if (iUser.findOne(UserId) == null) {
            return "NoRecordToEdit";
        }
        UserFormModel userFormModel = new UserFormModel(user);
        model.addAttribute("editedUser", userFormModel);
        return "editUser";
    }

    @PostMapping(path = "/saveEditedUser/{id}")
    public String saveEditedContent(@PathVariable("id") Long id, UserFormModel userFormModel) {
        User entity = iUser.findOne(id);
        if (entity == null) {
            return "NoRecordToEdit";
        }
        entity.setLogin(userFormModel.getLogin());
        entity.setPassword(passwordEncoder.encode(userFormModel.getPassword()));
       // entity.setPassword(userFormModel.getPassword());
        entity.setAuthorityRole(userFormModel.getAuthorityRole());

        iUser.save(entity);

        return "redirect:/userList";
    }
}
