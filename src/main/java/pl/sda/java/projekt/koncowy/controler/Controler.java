package pl.sda.java.projekt.koncowy.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.sda.java.projekt.koncowy.entity.Task;
import pl.sda.java.projekt.koncowy.model.TaskFormModel;
import pl.sda.java.projekt.koncowy.repository.ITask;
import pl.sda.java.projekt.koncowy.service.Priority;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Controller
public class Controler {

    @Autowired
    ITask iTask;

    @RequestMapping(value = "/list")
    public String view(Model model,@PageableDefault(size = 5) Pageable pageable ) {
        model.addAttribute("lista", iTask.findAllByIsComplectedFalseOrderByDataAsc(pageable));

        return "list";
    }

    @GetMapping(path = "/editTask/{TaskId}")
    public String editTask(@PathVariable("TaskId") Long TaskId, Model model) {
        Task task = iTask.findOne(TaskId);
        if (iTask.findOne(TaskId) == null) {
            return "NoRecordToEdit";
        }
        TaskFormModel taskFormModel = new TaskFormModel(task);
        model.addAttribute("editedTask", taskFormModel);
        return "editTask";
    }


    @GetMapping(path = "/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        if (iTask.findOne(id) == null) {
            return "NoRecordToDelete";
        }
        iTask.delete(id);
        return "redirect:/list";
    }

    @GetMapping(path = "/add")
    public String addTask(Model model) {
        Task newTask = new Task();
        model.addAttribute("newTask", newTask);
        return "addTask";
    }


    @RequestMapping(value = "/saveNewTask", method = RequestMethod.POST)
    public String saveNewTask(@Validated Task newTask, BindingResult errors) {
        if (errors.hasErrors()) {
            return "addTask"; //w przypadku zwrócenia error, widok zostaje ten sam
        }
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

        try {
            newTask.setData(df.parse(newTask.getScreenData()));
        } catch (ParseException e) {
        }
        iTask.save(newTask);
        return "redirect:/list";
    }


    @ModelAttribute("prioritylist")
    public Priority[] prioritylist() {
        return Priority.values();
    }

    @PostMapping(path = "/saveEditedContent/{id}")
    public String saveEditedContent(@PathVariable("id") Long id, TaskFormModel editedTask) {
        Task entity = iTask.findOne(id);
        if (entity == null) {
            return "NoRecordToEdit";
        }
        entity.setContent(editedTask.getContent());
        entity.setPriority(editedTask.getPriority());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

        try {
            entity.setData(df.parse(editedTask.getScreenData()));
        } catch (ParseException e) {
        }


        iTask.save(entity);

        return "redirect:/list";
    }

    @PostMapping(path = "/saveNewTask/")
    public String saveNewTask(@Validated @ModelAttribute("newTask") TaskFormModel newTask, BindingResult bindingResult) {
        /*Optional.ofNullable(newTask.getId())
                .flatMap(email -> iTask.findFirstById(newTask.getId()))
                .ifPresent(existingUser -> bindingResult.addError(new FieldError("newUser", "email",newTask.getId(),
                        false, new String[] {"emailExists"}, new Object[] {}, "User with this email already exists")));*/
        if (bindingResult.hasErrors()) {
            return "addTask";
        }
        Task entity = new Task();
        newTask.getContent();
        newTask.getScreenData();
        newTask.getData();
        newTask.getPriority();

        iTask.save(entity);
        return "redirect:/list";
    }
    @PostMapping(path = "/update/{id}")
    public String updateTask(@PathVariable("id") Long id, TaskFormModel updateFormModel){
        Task entity = iTask.findOne(id);
        if (entity==null){
            return "badGateway";
        }
        entity.setComplected(true);
        iTask.save(entity);
        return "redirect:/list";
    }
}