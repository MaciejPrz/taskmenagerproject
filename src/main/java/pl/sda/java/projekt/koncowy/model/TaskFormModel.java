package pl.sda.java.projekt.koncowy.model;

import org.hibernate.validator.constraints.NotEmpty;
import pl.sda.java.projekt.koncowy.entity.Task;
import pl.sda.java.projekt.koncowy.service.Priority;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TaskFormModel {
    private Long id;

    @NotEmpty
    private String content;

    @NotNull
    private Date data;

    @NotEmpty
    private String screenData;

    @NotNull
    private Priority priority;

    public TaskFormModel() {
    }
    @NotNull
    private boolean isComplected = false;

    public TaskFormModel(Task task){
        this.id = task.getId();
        this.content=task.getContent();
        this.data=task.getData();


        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        if (task.getData() != null) {
            this.screenData = df.format(task.getData());
        }
        this.priority=task.getPriority();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getScreenData() {
        return screenData;
    }

    public void setScreenData(String screenData) {
        this.screenData = screenData;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public boolean isComplected() {
        return isComplected;
    }

    public void setComplected(boolean complected) {
        isComplected = complected;
    }

}

