package pl.sda.java.projekt.koncowy.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String content;

    @Column
    private Date data;

    @Transient
    private String screenData;

    @Enumerated(EnumType.STRING)
    private pl.sda.java.projekt.koncowy.service.Priority priority;

    @Column
    private boolean isComplected = false;

    public Task() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getScreenData() {
        return screenData;
    }

    public void setScreenData(String screenData) {
        this.screenData = screenData;
    }

    public pl.sda.java.projekt.koncowy.service.Priority getPriority() {
        return priority;
    }

    public void setPriority(pl.sda.java.projekt.koncowy.service.Priority priority) {
        this.priority = priority;
    }

    public boolean isComplected() {
        return isComplected;
    }

    public void setComplected(boolean complected) {
        isComplected = complected;
    }
}
