/*
package pl.sda.java.projekt.koncowy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import pl.sda.java.projekt.koncowy.controler.LoginControler;
import pl.sda.java.projekt.koncowy.controler.UserControler;
import pl.sda.java.projekt.koncowy.entity.User;
import pl.sda.java.projekt.koncowy.model.TaskFormModel;
import pl.sda.java.projekt.koncowy.model.UserFormModel;
import pl.sda.java.projekt.koncowy.repository.IUser;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;


@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserService {

    private UserFormModel userFormModel;

    private TaskFormModel taskFormModel;

    @Autowired
    private LoginControler loginControler;
    @Autowired
    private IUser iUser;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void initSesja(){
        newUser();
    }
    public void newUser(){
        userFormModel = new UserFormModel();

    }
    @Transactional
    public void saveUser(){

        User entity =  new User();
        entity.setLogin(userFormModel.getLogin());
        entity.setPassword(passwordEncoder.encode(userFormModel.getPassword()));
        entity.setAuthorityRole(userFormModel.getAuthorityRole());

        entity = iUser.save(entity);
    }
}
*/
