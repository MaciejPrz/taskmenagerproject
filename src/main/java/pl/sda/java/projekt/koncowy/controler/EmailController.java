package pl.sda.java.projekt.koncowy.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import pl.sda.java.projekt.koncowy.service.EmailService;

@Controller
public class EmailController {

    @Autowired
    private EmailService emailService;

    @GetMapping("/email")
    public void sendEmail(){
        emailService.sendEmail("kaczka997@wp.pl","Test","lool");
    }
}
