package pl.sda.java.projekt.koncowy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmail(String email, String subject, String message){
        MimeMessage  mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);

        try {
            helper.setTo(email);
            helper.setSubject(subject);
            helper.setText(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        javaMailSender.createMimeMessage();
    }
    @Scheduled(fixedRate = 2000) //kod crona cron = "0 1 * * * MON-FRI
    public void runScheduledTask(){

    }
    //dodanie cyklicznych powtarzalnych metod, po dodaniu loga będzie widać w konsoli

    @Async
    public void longStoryTask(){
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    // tworzy nowy wątek w którym są wykonywane długi wątki
}
