package pl.sda.java.projekt.koncowy.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.java.projekt.koncowy.entity.User;
import java.util.List;

@Repository
public interface IUser extends CrudRepository<User,Long> {
    List<User> findAllByOrderById();
}
