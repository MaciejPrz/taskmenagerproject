package pl.sda.java.projekt.koncowy.model;

import org.hibernate.validator.constraints.NotEmpty;
import pl.sda.java.projekt.koncowy.entity.User;
import pl.sda.java.projekt.koncowy.service.AuthorityRole;
import javax.validation.constraints.NotNull;

public class UserFormModel {
    private Long id;

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;

    @NotNull
    private AuthorityRole authorityRole;

    public UserFormModel(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.authorityRole = user.getAuthorityRole();
    }

    public Long getId() {
        return id;
    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AuthorityRole getAuthorityRole() {
        return authorityRole;
    }

    public void setAuthorityRole(AuthorityRole authorityRole) {
        this.authorityRole = authorityRole;
    }

    public UserFormModel() {
    }
}


