package pl.sda.java.projekt.koncowy.service;

public enum Priority {
    LOW("Low"), MEDIUM("Medium"), HIGHT("Hight");

    private String name;

    Priority(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
