package pl.sda.java.projekt.koncowy.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        /*authenticationManagerBuilder.inMemoryAuthentication().withUser("admin").password("admin")
                .roles("manager","user").and().withUser("user").password("user1").roles("user");
        authenticationManagerBuilder.inMemoryAuthentication().withUser("null").password("").roles("admin");*/
        authenticationManagerBuilder.jdbcAuthentication()
                .dataSource(dataSource)
                // .passwordEncoder() do szyfrowania
                .authoritiesByUsernameQuery("SELECT u.login, u.authority_role FROM project.user u WHERE u.login=?")
                .usersByUsernameQuery("SELECT u.login, u.password, 1 FROM project.user u WHERE u.login=?");

    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().antMatchers("/list").permitAll()
                 .antMatchers("/add/**").authenticated()
                .antMatchers("/delete/**").authenticated()
                .antMatchers("/editTask/**").authenticated()
                .and().formLogin()
                //gwiazdka zastępuje np w tym wypadku usuwanego id można zastosować /**/* zastepuje url strony na dowolny
                //dodanie logowania
                .loginProcessingUrl("/login")
                .loginPage("/loginForm")
                .defaultSuccessUrl("/list")
                .failureUrl("/loginFailure")
                .usernameParameter("login")
                .passwordParameter("password").and()
                .logout().logoutUrl("/logout").logoutSuccessUrl("/list").and()
                .exceptionHandling().accessDeniedPage("/accessDenied");

    }

    @Bean
    public PasswordEncoder myPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}